$(document).ready(function(){
    var haut = 10;
    $("#cache").click(function(){
        $("#bloc").hide();
    });
    $("#retour").click(function(){
        $("#bloc").show();
    });
    $("#vert").click(function(){
        $("#bloc").css("background-color", "green");
    });
    $("#base").click(function(){
        $("#bloc").css("background-color", "black");
    });
    $("#hauteur").click(function(){
        haut += 10;
        $("#bloc").css("height", + haut + "px");
        if(haut === 100){
            haut= 10;
            $("#bloc").animate({height: 5}, 1100);         
        }
    });
});